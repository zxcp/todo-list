$(document).ready ->
    $('.js-delete').click ->
        task.deleteWithConfirm this
        return
    $('.js-create').click ->
        task.create()
        return
    updateIndexes = ->
        $('tbody > tr').each (i) ->
            $(this).find('th').text i + 1
            return
        return
    task =
        create: ->
            Swal.fire(
                title: 'Введите текст задачи'
                input: 'text'
                inputAttributes:
                    autocapitalize: 'off'
                showCancelButton: true
                confirmButtonText: 'Создать'
                cancelButtonText: 'Отмена').then (result) ->
                    if !result.isConfirmed
                        return
                    $.ajax
                        url: 'api/create'
                        method: 'POST'
                        data:
                            text: result.value,
                            index: $('tbody').find('tr').length
                        statusCode:
                            200: (response) ->
                                tbody = $('tbody');
                                tbody.append response
                                tr = tbody.last()
                                tr.find('button').click ->
                                    task.deleteWithConfirm this
                                    return
                                Swal.fire 'Задача создана', '', 'success'
                                return
                            204: () ->
                                Swal.fire 'Текст задачи пуст', '', 'error'
                                return
                    return
            return
        deleteWithConfirm: (elem) ->
            Swal.fire(
                title: 'Вы уверены?'
                icon: 'warning'
                showCancelButton: true
                confirmButtonColor: '#d33'
                cancelButtonColor: '#3085d6'
                confirmButtonText: 'Удалить'
                cancelButtonText: 'Отмена').then (result) ->
                    if !result.isConfirmed
                        return
                    button = $(elem)
                    $.ajax
                        url: 'api/delete'
                        method: 'DELETE'
                        data:
                            id: button.val()
                        statusCode:
                            200: ->
                                Swal.fire 'Задача удалена', '', 'success'
                                button.closest('tr').remove()
                                updateIndexes()
                                return
                            404: ->
                                Swal.fire 'Задача не найдена', '', 'error'
                                return
                    return
    return
return
