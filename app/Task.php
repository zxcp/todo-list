<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 * @package App
 *
 * @property int $id
 * @property string $text
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Task extends Model
{
    public function getDateString()
    {
        return $this->created_at->format('d.m.Y H:i:s');
    }
}
