<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class App extends Controller
{
    /**
     * Список задач
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        return view('app.list', [
            'tasks' => Task::all()
        ]);
    }

    /**
     * Создание задачи
     *
     * @param Request $request
     * @return Response возвращает html элемента таблицы с новой задачей
     */
    public function create(Request $request)
    {
        $text = (string)$request->post('text');
        $index = (int)$request->post('index');//Порядковый номер записи в таблице

        if (!$text) new Response('Текст задачи пуст', 204);

        $task = new Task();
        $task->text = $text;
        $task->save();

        return view('app.tr', [
            'task' => $task,
            'index' => $index
        ]);
    }

    /**
     * Удаление задачи
     *
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request)
    {
        $task = Task::find((int)$request->get('id'));
        if ($task) {
            $task->delete();

            return new Response('Задача удалена');
        }

        return new Response('Задача не найдена', 404);
    }
}
